## Description
Automatic Fetching System:

Full bash shell version
 
Piggyback.sh is a software developed to retrieve data from a remote server:

1. Parse URL (HTML URL information from http://147.96.21.177/). 

2. List all available URL links from server recursively.

3. Display all available URL links on screen.

4. Retrieve entire URL recursively.

5. Retrieve URLs from previous two nights.

6. Retrieve a unique URL by date YYYYMMDD/.


## Note:

To display help, type in the terminal: ./piggyback.sh -h 

Crontab executes piggyback.sh every day at 16:20 (local time: Madrid).

Software for Unix-like systems developed by:

Jose Robles, josrob01@ucm.es